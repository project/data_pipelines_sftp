<?php

declare(strict_types=1);

namespace Drupal\data_pipelines_sftp\Source\Resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Source\Resource\SourceResourceBase;
use Drupal\data_pipelines_sftp\Storage;
use Drupal\key\KeyRepositoryInterface;
use phpseclib3\Net\SFTP as SFTPClient;
use phpseclib3\Net\SFTP\Stream;

/**
 * Provides SFTP a source resource.
 */
class Sftp extends SourceResourceBase {

  /**
   * The dataset.
   *
   * @var Drupal\data_pipelines\Entity\DatasetInterface
   */
  protected DatasetInterface $dataset;

  /**
   * The field name.
   *
   * @var string
   */
  protected string $fieldName;

  public function __construct(protected KeyRepositoryInterface $keyRepository, protected Storage $storage) {}

  /**
   * {@inheritdoc}
   */
  public function getResource(DatasetInterface $dataset, string $field_name): mixed {
    $this->dataset = $dataset;
    $this->fieldName = $field_name;
    $path = $this->getPath();
    try {
      $client = $this->getClient();
      $contents = $client->get($path);
      if (!$contents) {
        throw new \Exception(sprintf("Unable to retrieve contents from %s", $path));
      }
      return $this->requireLocalCopy() ? $this->storage->createLocalCopy($path, $contents) : $this->storage->createMemoryCopy($contents);
    }
    catch (\Exception $e) {
      $dataset->addLogMessage(sprintf("There was an error getting the file from the file server - %s", $e->getMessage()));
    }
    if ($this->requireLocalCopy()) {
      $dataset->addLogMessage(sprintf("The local copy of %s has been used.", $path));
      return $this->storage->getLocalCopy($path);
    }
    return NULL;
  }

  /**
   * A method to get the file path.
   *
   * @return string
   *   The file path.
   */
  protected function getPath(): string {
    return self::getFieldValue($this->dataset, $this->fieldName);
  }

  /**
   * A method to determine whether a local copy is required.
   *
   * @return bool
   *   The result.
   */
  protected function requireLocalCopy(): bool {
    return (bool) self::getFieldValue($this->dataset, $this->fieldName, 'local_copy');
  }

  /**
   * A method to get the host.
   *
   * @return string
   *   The host.
   */
  protected function getHost(): string {
    return self::getFieldValue($this->dataset, $this->fieldName, 'host');
  }

  /**
   * A method to get the port.
   *
   * @return int
   *   The port.
   */
  protected function getPort(): int {
    $port = (int) self::getFieldValue($this->dataset, $this->fieldName, 'port');
    return $port ?: 22;
  }

  /**
   * A method to get the credentials.
   *
   * @return string
   *   The credentials.
   */
  protected function getCredentials(): string {
    return self::getFieldValue($this->dataset, $this->fieldName, 'credentials');
  }

  /**
   * A method to get the authenticated SFTP client.
   *
   * @return \phpseclib3\Net\SFTP
   *   The authenticated SFTP client.
   *
   * @throws \Exception
   */
  protected function getClient(): SFTPClient {
    static $client;
    if ($client) {
      return $client;
    }
    Stream::register();
    $client = new SFTPClient($this->getHost(), $this->getPort());
    ['username' => $username, 'password' => $password] = Json::decode($this->keyRepository->getKey($this->getCredentials())->getKeyValue());
    if (!$client->login($username, $password)) {
      throw new \Exception('Authentication failed.');
    }
    return $client;
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceBaseFieldDefinition(array $source_plugin_definition): BaseFieldDefinition {
    return BaseFieldDefinition::create('sftp')
      ->setLabel(new TranslatableMarkup('SFTP details'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'sftp',
      ]);
  }

}
