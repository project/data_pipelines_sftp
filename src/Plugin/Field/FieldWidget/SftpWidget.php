<?php

declare(strict_types=1);

namespace Drupal\data_pipelines_sftp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the SFTP field widget.
 *
 * @FieldWidget(
 *    id = "sftp",
 *    label = @Translation("SFTP"),
 *    field_types = {
 *      "sftp"
 *    },
 *  )
 */
class SftpWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path on the server'),
      '#default_value' => $items[$delta]->path,
      '#required' => TRUE,
    ];
    $element['local_copy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Local copy'),
      '#description' => $this->t("Create a local copy of the file. In the event of the remote file not being available, this copy will be used."),
      '#default_value' => $items[$delta]->local_copy,
    ];
    $element['connection'] = [
      '#type' => 'details',
      '#title' => $this->t('Connection'),
    ];
    $element['connection']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $items[$delta]->host,
      '#required' => TRUE,
    ];
    $element['connection']['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#size' => 4,
      '#default_value' => $items[$delta]->port ?? NULL,
      '#attributes' => [
        'placeholder' => 22,
      ],
    ];
    $element['connection']['credentials'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Key'),
      '#key_filters' => ['type' => 'user_password'],
      '#default_value' => $items[$delta]->credentials ?? NULL,
      '#required' => TRUE,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    foreach ($values as &$value) {
      $value = [...$value, ...$value['connection']];
    }
    return $values;
  }

}
