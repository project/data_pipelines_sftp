<?php

declare(strict_types=1);

namespace Drupal\data_pipelines_sftp\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides the SFTP field type.
 *
 * @FieldType(
 *    id = "sftp",
 *    label = @Translation("SFTP"),
 *    default_widget = "sftp",
 *    no_ui = TRUE
 *  )
 */
class SftpItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = [];
    $properties['path'] = DataDefinition::create('string')
      ->setLabel(t('Path'));
    $properties['host'] = DataDefinition::create('string')
      ->setLabel(t('Host'));
    $properties['port'] = DataDefinition::create('string')
      ->setLabel(t('Port'));
    $properties['credentials'] = DataDefinition::create('string')
      ->setLabel(t('Credentials'))
      ->setDescription(t('The credentials key.'));
    $properties['local_copy'] = DataDefinition::create('boolean')
      ->setLabel(t('Local copy'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'path' => [
          'type' => 'varchar',
          'length' => 512,
        ],
        'host' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'port' => [
          'type' => 'varchar',
          'length' => 32,
        ],
        'credentials' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'local_copy' => [
          'type' => 'int',
          'size' => 'tiny',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): string {
    return 'path';
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update): void {
    parent::postSave($update);
    if (!$this->values['local_copy']) {
      /** @var \Drupal\data_pipelines_sftp\Storage $storage */
      $storage = \Drupal::service('data_pipelines_sftp.storage');
      $storage->deleteLocalCopy($this->values['path']);
    }
  }

}
