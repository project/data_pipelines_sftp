<?php

declare(strict_types=1);

namespace Drupal\data_pipelines_sftp;

use Drupal\Core\File\FileSystemInterface;

/**
 * Provides a storage class for local files and memory.
 */
class Storage {

  public function __construct(protected FileSystemInterface $fileSystem) {}

  /**
   * The local folder for storage.
   *
   * @var string
   */
  const LOCAL_FOLDER = 'private://data_pipelines_sftp/cache';

  /**
   * A method to get the local path for storage.
   *
   * The local path is derived from the original path to the file. The filename
   * is extracted.
   *
   * @param string $path
   *   The original path to the file.
   *
   * @return string
   *   The local path for the file.
   */
  public function getLocalPath(string $path): string {
    return self::LOCAL_FOLDER . DIRECTORY_SEPARATOR . pathinfo($path, PATHINFO_BASENAME);
  }

  /**
   * A method to prepare a local path for storage.
   *
   * @param string $path
   *   The local path.
   *
   * @return bool
   *   The result.
   */
  public function prepareLocalPath(string $path): bool {
    $local_path = $this->getLocalPath($path);
    $directory = pathinfo($local_path, PATHINFO_DIRNAME);
    return $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
  }

  /**
   * A method to delete a local copy of a file.
   *
   * @param string $path
   *   The original path to the file.
   *
   * @return bool
   *   The result.
   */
  public function deleteLocalCopy(string $path): bool {
    return $this->fileSystem->unlink($this->getLocalPath($path));
  }

  /**
   * A method to create a local copy of a file.
   *
   * @param string $path
   *   The original path to the file.
   * @param string $contents
   *   The contents of the file.
   *
   * @return mixed
   *   The resource of the local file.
   *
   * @throws \Exception
   */
  public function createLocalCopy(string $path, string $contents): mixed {
    $this->prepareLocalPath($path);
    if ($stream = fopen($this->getLocalPath($path), 'w+')) {
      if (!fwrite($stream, $contents)) {
        throw new \Exception('Could not create local copy.');
      }
      rewind($stream);
      return $stream;
    }
    return NULL;
  }

  /**
   * A method to get the local copy of a file.
   *
   * @param string $path
   *   The original path to the file.
   *
   * @return mixed
   *   The resource of the local file.
   */
  public function getLocalCopy(string $path): mixed {
    if (is_readable($this->getLocalPath($path))) {
      if ($stream = fopen($path, 'r')) {
        rewind($stream);
        return $stream;
      }
    }
    return NULL;
  }

  /**
   * A method to create an in-memory copy of a file.
   *
   * @param string $contents
   *   The contents of the file.
   *
   * @return mixed
   *   The resource of the in-memory file.
   *
   * @throws \Exception
   */
  public function createMemoryCopy(string $contents): mixed {
    if ($stream = fopen('php://temp', 'w+')) {
      if (!fwrite($stream, $contents)) {
        throw new \Exception('Could not create memory copy.');
      }
      rewind($stream);
      return $stream;
    }
    return NULL;
  }

}
