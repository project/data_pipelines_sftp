<?php

declare(strict_types=1);

namespace Drupa\Tests\data_pipelines_sftp\Unit\Source\Resource;

use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines_sftp\Source\Resource\Sftp;
use Drupal\data_pipelines_sftp\Storage;
use Drupal\key\KeyRepositoryInterface;
use phpseclib3\Net\SFTP as SFTPClient;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Drupal\data_pipelines_sftp\Source\Resource\Sftp
 */
class SftpTest extends TestCase {

  /**
   * A method to get the mocked Sftp class.
   *
   * @return PHPUnit\Framework\MockObject\MockObject|Drupal\data_pipelines_sftp\Source\Resource\Sftp
   *   The mocked Sftp class.
   */
  protected function getBaseMock(Storage $storage): MockObject | Sftp {
    $mock = $this->getMockBuilder(Sftp::class)
      ->onlyMethods([
        'getPath',
        'requireLocalCopy',
        'getHost',
        'getPort',
        'getCredentials',
        'getClient',
      ])
      ->setConstructorArgs([
        $this->createMock(KeyRepositoryInterface::class),
        $storage,
      ])
      ->disableOriginalClone()
      ->disableArgumentCloning()
      ->disallowMockingUnknownTypes()
      ->getMock();
    $mock->expects($this->any())
      ->method('getPath')
      ->willReturn('foo/bar.json');
    $mock->expects($this->any())
      ->method('getHost')
      ->willReturn('127.0.0.1');
    $mock->expects($this->any())
      ->method('getPort')
      ->willReturn(22);
    $mock->expects($this->any())
      ->method('getCredentials')
      ->willReturn('123456789');
    return $mock;
  }

  /**
   * A method to get dummy data.
   *
   * @return string
   *   The data.
   */
  protected function getData(): string {
    return '{"foo": "bar"}';
  }

  /**
   * A method to test getting a resource using memory (default).
   *
   * The underlying storage method createMemoryCopy must be called.
   *
   * @covers \Drupal\data_pipelines_sftp\Source\Resource\Sftp::getResource
   */
  public function testGetResource(): void {
    $storage = $this->createMock(Storage::class);
    $storage->expects($this->once())
      ->method('createMemoryCopy');
    $mock = $this->getBaseMock($storage);
    $dataset = $this->createMock(DatasetInterface::class);

    // Memory write success.
    $client = $this->createMock(SFTPClient::class);
    $client->expects($this->once())
      ->method('get')
      ->withAnyParameters()
      ->willReturn($this->getData());
    $mock->expects($this->once())
      ->method('getClient')
      ->willReturn($client);
    $mock->getResource($dataset, 'foo_bar');
  }

  /**
   * A method to test getting a resource using a local copy.
   *
   * The underlying storage method createLocalCopy must be called.
   *
   * @covers \Drupal\data_pipelines_sftp\Source\Resource\Sftp::getResource
   */
  public function testGetResourceRequireLocal(): void {
    $storage = $this->createMock(Storage::class);
    $storage->expects($this->once())
      ->method('createLocalCopy');
    $mock = $this->getBaseMock($storage);
    $dataset = $this->createMock(DatasetInterface::class);
    $client = $this->createMock(SFTPClient::class);
    $client->expects($this->any())
      ->method('get')
      ->withAnyParameters()
      ->willReturn($this->getData());
    $mock->expects($this->any())
      ->method('getClient')
      ->willReturn($client);
    $mock->expects($this->any())
      ->method('requireLocalCopy')
      ->willReturn(TRUE);
    $mock->getResource($dataset, 'foo_bar');
  }

  /**
   * A method to test getting a resource with failed authentication.
   *
   * @covers \Drupal\data_pipelines_sftp\Source\Resource\Sftp::getResource
   */
  public function testGetResourceAuthenticationFail(): void {
    $storage = $this->createMock(Storage::class);
    $mock = $this->getBaseMock($storage);
    $dataset = $this->createMock(DatasetInterface::class);
    $mock->expects($this->any())
      ->method('getClient')
      ->willThrowException(new \Exception('Authentication failed.'));
    $resource = $mock->getResource($dataset, 'foo_bar');
    $this->assertFalse(is_resource($resource));
  }

}
