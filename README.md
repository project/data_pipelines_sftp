# Data Pipelines SFTP

Provides SFTP support to Data Pipelines.

## Features
At present the module allows a user to add a dataset using a file server as a resource for the supported sources, i.e. JSON. The supported protocol is SFTP.

## Additional Requirements
The module directly depends on Data Pipelines and Key.

Key is required in order to add a username/password pair that will be available as an option when configuring the connection details of the dataset.